# Styled components

La bibliothèque "styled-components" est une bibliothèque pour React qui permet de styliser les composants de manière dynamique en utilisant des CSS-in-JS (CSS dans JavaScript). En d'autres termes, elle permet de créer des composants React avec des styles CSS définis directement dans le code JavaScript.

Plutôt que d'avoir des fichiers CSS externes, avec "styled-components", les styles sont directement inclus dans le composant lui-même. Cela facilite la gestion des styles et permet de définir des styles en fonction de l'état du composant, des propriétés passées en paramètres, ou même de manière conditionnelle.

La bibliothèque "styled-components" utilise une syntaxe de template literal pour définir les styles. Cela signifie que les styles sont définis dans une chaîne de caractères multiligne (entre ``), qui peut inclure des variables, des expressions JavaScript et des fonctions. Cette approche permet de créer des styles dynamiques et réutilisables pour les composants.

[Lien temporaire](https://styled-components.com/)
