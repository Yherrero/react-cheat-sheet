# Pourquoi utiliser Tailwind CSS pour styliser son app React ?

## Introduction

Tailwind fait partie de la même catégorie que TypeScript ou Svelte. On comprend leur intérêt qu'une fois qu'on les a essayés, et une fois qu'on les a essayés, il est impossible de revenir en arrière.

Tailwind CSS est un framework CSS utilitaire qui simplifie le développement en fournissant un ensemble de classes pré-définies pour styler les composants, éliminant ainsi le besoin de créer des classes CSS personnalisées. Il favorise une approche de développement rapide et modulaire en utilisant des classes directement dans le template pour définir l'apparence des éléments.

## Nommer ses classes

On sait tous qu'une des parties les plus ennuyeuses du code, c'est de trouver des noms. Et plus spécialement pour les classes CSS.

On commence d'abord par un scope réduit, par exemple `.page-header` pour se rendre compte plus tard que les propriétés CSS utilisées correspondent aussi au `.panel-title`. Ou alors, au contraire, on commence avec un scope trop large pour se rendre compte par la suite qu'il faut le réduire ou le combiner avec des classes utilitaires.

Il existe des solutions pour ces problèmes comme BEM, par exemple (une convention de nommage). Mais elles sont généralement très verbeuses et pas forcément agréables à utiliser.

Tailwind permet de supprimer la partie de nommage dans la quasi-totalité des cas. Encore plus lorsqu'il est utilisé avec des composants.

Par exemple, imaginons que vous devez créer un nouveau composant pour une carte et que vous voulez styliser le header. Vous pouvez créer une nouvelle classe `.card__header` puis définir les propriétés CSS et faire la même chose pour le composant enfant.

Même si cette solution fonctionne, les chances que vous réutilisiez cette classe ailleurs dans l'application sont très faibles. Avec une approche basée sur Tailwind, vous vous retrouverez avec quelque chose comme ça `<div class="text-xl text-gray-500">` et c'est tout.

## Code reviews

Un des problèmes quand on review du code front, c'est que le code est dispersé à plusieurs endroits. Par exemple, si une nouvelle classe est ajoutée à un élément, il faudra vérifier la partie template en plus de la partie CSS, qui se trouvent à deux endroits différents du code.

Cela complique les code reviews, et encore plus quand une classe existante est modifiée et que vous n'êtes pas sûr de comment cela affecte le reste de l'application.

Les code reviews sont énormément simplifiées lorsque l'on utilise des classes utilitaires directement dans le template. On peut de suite voir ce qui a changé en se basant sur les classes utilisées sans avoir à regarder ailleurs. En plus de ça, il n'y a pas de risque que les changements effectués affectent une autre partie de l'application sans que cela soit voulu.

## Fait pour les composants

Tailwind est idéal quand il est utilisé avec des composants. Il permet d'améliorer la réutilisabilité du code.

Généralement, il existe deux façons d'approcher la réutilisabilité du style : les classes CSS et les composants.

Tailwind étant un framework basé sur des classes utilitaires, cela permet de se concentrer sur la réutilisabilité des composants. Et donc, au lieu de créer des classes CSS qui seront partagées sur plusieurs composants, il faudra plutôt créer directement un nouveau composant.

Moins de règles à suivre et une couche d'abstraction en moins.

## Optimisation du bundle

Il arrive souvent lorsque l'on doit ajouter un nouvel élément dans notre template qu'il faille aussi lui ajouter du style. Dans la majorité des cas, on crée une nouvelle classe avec les propriétés liées sans se rendre compte qu'il y a déjà 3 autres classes qui contiennent exactement les mêmes styles.

Un autre cas est lorsqu'il faut changer le style pour un élément en particulier. Au moment de modifier la classe, de peur de changer un élément ailleurs dans l'application, on se retrouve à créer une autre classe avec les mêmes propriétés plus le changement voulu.

À force, on se retrouve avec un grand nombre de classes inutilisées ou dupliquées dans le code, ce qui résulte en un bundle beaucoup plus large que nécessaire.

Un des points forts de Tailwind est qu'il effectue une purge automatique des styles inutilisés, éliminant ainsi tout code non essentiel, ce qui permet d'optimiser la taille des fichiers.

## Librairies UI externes

Un autre grand avantage d'utiliser Tailwind est la grande diversité de librairies UI disponibles ainsi que leurs possibilités de personnalisation.

Contrairement à des librairies comme Ant Design ou MUI qui rendent la modification des styles par défaut assez compliquée et peu intuitive, les librairies Tailwind sont généralement personnalisables de façon extrêmement simple en ajoutant juste des classes Tailwind.

Par exemple, si on utilise la librairie Shadcn UI et que l'on souhaite modifier le border-radius du composant Button, il suffit juste de rajouter le style souhaité comme ceci : `<Button className="rounded-full">`. On peut ainsi ajouter n'importe quel style et modifier ce que l'on souhaite pour profiter des composants offerts par la librairie tout en obtenant le style exact que l'on souhaite.

À noter que l'utilisation de Tailwind ne bloque en aucun cas l'utilisation des librairies UI classiques comme Antd ou MUI.

Voici une liste non-exhaustive des principales librairies UI adaptées à Tailwind :

Shadcn UI
NextUi
Tailwind UI
Preline UI
Daisy UI

Il existe aussi des librairies de composants non stylisés qui peuvent être pratiques lorsque l'on souhaite avoir des composants complexes (tableau, dialog, popover...) qui viennent sans aucun style par défaut. Les deux principales sont Radix UI et Headless UI.

## Intégration dans les IDE et extensions

Il existe des extensions pour la plupart des IDE permettant d'avoir l'auto-complétion ainsi que la prévisualisation des couleurs et qui améliorent généralement l'expérience utilisateur.

- prettier Tailwind (Permet d'ajouter de formater l'ordre des classes Tailwind automatiquement grâce à prettier.)
- [neovim](https://github.com/roobert/tailwindcss-colorizer-cmp.nvim)
- [vs code extension](https://github.com/tailwindlabs/tailwindcss-intellisense)
