# Utiliser React Router

[React router](https://reactrouter.com/en/main/start/tutorial) est une librairie que l'on peut ajouter à React pour pouvoir ajouter un systeme de routing à son application.

# Installation

pour installer react router, il suffit de lancer la commande suivante à la racine de votre projet

```sh
npm install react-router-dom
```

# Initialisation du router et des routes

```js
// router.js
import { createBrowserRouter } from "react-router-dom";

import HomePage from "../pages/HomePage";
import LoginPage from "../pages/LoginPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
]);

export default router;
```

Ici, on à créer notre objet `router` et on à définie deux routes. Une route pour la page de login qui sera accessible sur l'url `localhost:3000/login` et une route pour la page d'accueil qui sera accessible sur l'url `localhost:3000`.  
Dans la partie `element` de notre route, il faut mettre le composant qui sera affiché lorsque on sera sur la page en question

# Utiliser le router

Pour utiliser le router, il faut donc l'importer et entourer notre application avec un `RouterProvider`. Généralement, ce `RouterProvider` est ajouté directement au niveau du fichier `App.js`

```js
// App.js
import { RouterProvider } from "react-router-dom";

import router from "./router/router";

const App = () => {
  return <RouterProvider router={router} />;
};

export default App;
```

On peut voir ici qu'on a donc importé le `RouterProvider` depuis `react-router-dom` et qu'on ajouter ce composant dans notre composant `App`. Le fonctionnement du router fait que les `element` associés aux routes que l'on à créées au préalable seront chargés à l'endroit où se trouve ce `RouterProvider`. Il ne faut pas oublier de lui donner en props le router que l'on à créer dans le fichier `router.js`

# Naviguer entre les pages

Pour naviguer entre les pages, on peut utiliser le composant `Link` fournit par `react-router-dom`

```js{7}
import { Link } from "react-router-dom";

const MyComponent = () => {
  return (
    <div>
      <h1>Aller sur la page de connexion</h1>
      <Link to="/login">Cliquez ici</Link>
    </div>
  );
};
```

On donne donc en props du `Link` le chemin auquel on veut accéder grâce à la props `to`  
Pour plus de détails sur le composant `Link` vous pouvez visiter [ce lien](https://reactrouter.com/en/main/components/link)

# Les routes dynamiques

Pour définir une route dynamique, dans le fichier `router.js` au moment de définir nos routes, on peut utiliser la syntaxe suivante `/route/:variable`. On donne donc le paramètre `variable` qui sera accessible dans la page user. On peut choisir le nom que l'on veut pour cette variable.

Par exemple, si on navigue sur l'url `localhost:3000/route/3`, la valeur de `variable` sera `3`.

```js
// router.js
const router = createBrowserRouter([
  // ...
  {
    path: "/user/:id",
    element: <UserPage />,
  },
]);
```

Maintenant que la route est définie, pour récupérer cette valeur dynamique sur notre route `user`, on peut utiliser le hook `useParams` fournit par `react-router-dom`

```js
// pages/UserPage.js
import { useParams } from "react-router-dom";

const UserPage = () => {
  const { userId } = useParams();

  return (
    <div>
      <h1>L'id de l'utilisateur est: {userId}</h1>
    </div>
  );
};
```

# Utilisation de layout

Imaginons qu'on ai besoin de développer une applications qui possede un menu disponible sur toute les pages. Au lieu d'importer ce composant dans chaque pages pour l'ajouter, il est possible d'utiliser un layout.

Un layout est juste un composant classique que l'on va utiliser pour englober le router et ainsi le rendre disponible peut importe la page sur laquelle l'utilisateur se trouve

```js
// src/layout/Layout.js

const Layout = ({ children }) => {
  return (
    <div>
      <header>Menu</header>
      {children}
    </div>
  );
};

export default Layout;
```

```js
// src/App.js
import { RouterProvider } from "react-router-dom";

import router from "./router/router";
import Layout from "./layout/Layout.js";

const App = () => {
  return (
    <Layout>
      <RouterProvider router={router} />
    </Layout>
  );
};

export default App;
```

# Note sur la structure de fichiers

Pour garder une architecture propre, voila un exemple d'une façon d'organiser ses fichier:

```
-src/
|--- App.js
|--- router/
|       |--- router.js
|--- layout/
|       |--- Layout.js
|--- pages/
        |--- HomePage.js
        |--- LoginPage.js
```

Ici, les composants qui representent des pages sont dans le dossier pages et les fichiers qui sont liés au router sont dans le dossier router
