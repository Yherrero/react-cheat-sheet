# Une librairie de composants, Ant Design

Il existe pour React des librairie de composants qui permettent de créer rapidement des interfaces utilisateurs cohérentes. Les plus utilisées sont principalement And Design (ou Antd) et Material UI (ou MUI).

Nous allons voir ici comment ajouter Antd à un projet React pour pouvoir bénéficier de tout les composants mis à disposition.

### Installation

Pour installer Antd, il suffit de lancer cette commande à la racine du projet

```sh
# npm
npm install antd

# yarn
yarn add antd
```

### Utilisation

Pour utiliser les composants de Antd dans un projet, il suffit de les importer depuis `antd`.

```js
import { Button, Space } from "antd";

const MyComponent = () => {
  return (
    <Space>
      <Button>Default Button</Button>
    </Space>
  );
};
```

[Liste des composants](https://ant.design/components/overview)
