# La gestion du store avec Zustand

Dans une application React, un "store" est un endroit centralisé pour stocker et gérer l'état global de l'application. Cela signifie que toutes les données importantes de l'application, telles que les informations de l'utilisateur, les préférences, les données récupérées depuis une API, sont stockées dans ce store.

En utilisant un store, l'état de l'application peut être partagé et mis à jour facilement entre les différents composants de l'application, sans avoir à passer des données de composant en composant. Cela facilite la gestion de l'état et rend l'application plus efficace.

### Initialiser son store

Dans un premier temps, il faut ajouter Zustand à notre application

```sh
# NPM
npm install zustand

# Yarn
yarn add zustand
```

une fois cela fait, on pourra créer un dossier `store` à la racine du dossier `src` à l'interieur duquel nous allons écrire tout le code lié au store.

Voici comment créer un store

```js
// src/store/counterStore.js

import { create } from "zustand";

export const useCounterStore = create((set) => ({
  count: 0,
  increaseCounter: () => set((state) => ({ count: state.count + 1 })),
  resetCounter: () => set({ count: 0 }),
}));
```

Dans cet exemple, on a créé un store qui contient une variable (state) `count` ainsi que deux fonctions (actions) `increaseCounter` et `resetCounter`

Dans un store, le state représente l'état et les actions servent à le modifier.

On remarque que comme pour le `useState`, lorsque l'on veut modifier une valeur du store en se basant sur sa valeur précédente, on utilise une fonction dans le `set` (comme pour l'action `increaseCounter`). Et lorsque l'on veut juste attribuer une valeur qui n'est pas lié à la valeur précédente on peut juste donner en parametre la valeur que l'on veut (comme dans `resetCounter`)

Voici comment utiliser le store que l'on vient de créer dans un composant

```js
// src/components/MyCounter.js
import { useCounterStore } from "../store/counterStore.js";

const MyCounter = () => {
  const count = useCounterStore((state) => state.count);

  const increaseCounter = useStore((state) => state.increaseCounter);

  return (
    <div>
      <h2>Valeur du compteur: {count}</h2>
      <button onClick={increaseCounter}>Incrémenter le compteur</button>
    </div>
  );
};

export default MyCounter;
```

### Faire persister le store

Zustand nous fournit une fonction qui peut s'ajouter à la déclaration de notre store pour le rendre persistant (Il ne se vide plus lorsque la page est rechargée)

```js
import { create } from "zustand";
import { persist } from "zustand/middleware";

export const useBearStore = create(
  persist(
    (set, get) => ({
      count: 0,
      increaseCounter: () => set({ count: get().count + 1 }),
    }),
    {
      name: "", // Nom de l'objet dans le localStorage (Doit être unique)
    }
  )
);
```

On voit dans cet exemple qu'il faut entourer le store de la fonction `persist` à qui il faut passer deux parametres:

- Le premier est la déclaration de notre store
- Le deuxieme est un objet d'options

on remarque que l'accès au state se change un peu par rapport à une définition de store non persistant. Au lieu d'avoir juste une fonction `set` et d'accéder au state avec `state.count`, une fonction `get` nous est fournit.

```js
// Avec un store non persistant
increaseCounter: () => set((state) => ({ count: state.count + 1 })),

// Avec un store persistant
increaseCounter: () => set({ count: get().count + 1 }),
```

[voir plus de détails sur la persistance](https://docs.pmnd.rs/zustand/integrations/persisting-store-data)
