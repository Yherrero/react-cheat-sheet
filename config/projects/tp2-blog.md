# Exercice 2 - Blog

## Instructions:

A partir des maquettes suivantes ([lien](https://www.figma.com/file/fesd8eNll4KvoYoXMbsQOi/Untitled?t=SXjvFBBMmlEiUD6E-6)) et de l'api [JsonPlaceholder](https://jsonplaceholder.typicode.com/)

Il faut développer un blog qui possède trois pages

### Page d'accueil

La page d'accueil contient une liste des utilisateurs.

Au click sur un des utilisateur, on est redirigé sur la page utilisateur qui va lister les articles écrits par cet utilisateur

### Page utilisateur

La page utilisateur liste tout les articles écrits par l'utilisateur selectionné.

Au click sur un article, on est redirigé sur la page article qui va montrer le contenue de l'article ainsi que tout les commentaires liés à cet article

### Page article

La page article montre les détails de l'article selectionné ainsi que tout ses commentaires liés. Un bouton se situe en bas de page pour rediriger l'utilisateur sur la page d'accueil

---

# Correction

Vous pourrez retrouver la correction sur [ce lien](https://drive.google.com/drive/folders/1Cd5tUvyuyfdzk1BnpbBs4stA_PvQnQX_?usp=share_link)
