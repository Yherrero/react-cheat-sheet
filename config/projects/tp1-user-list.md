# Exercice 1 - Liste d'utilisateurs

### Instructions:

<pre>
- Formulaire
- nom (texte)
- age (nombre)
- bouton -> créer l'utilisateur

- liste d'utilisateurs
- si aucun utilisateurs, ne s'affiche pas
- cliquer sur un utilisateur le retire de la liste

-> formulaire
- vider les champs au moment ou le form est submit
- ajouter les verifications
  - nom et age doivent être remplis
  - age > 0
- Si le formulaire n'est pas valide
  - afficher un message d'erreur
  - le message soit différent en fonction du type d'erreur
     - les champs ne doivent pas être vide
     - l'age doit être plus grand que 0

- style au choix
- styled-components, modules.css ou css classique
</pre>

---

# Correction

Vous pourrez retrouver la correction sur [ce lien](https://drive.google.com/drive/folders/1Cd5tUvyuyfdzk1BnpbBs4stA_PvQnQX_?usp=share_link)
