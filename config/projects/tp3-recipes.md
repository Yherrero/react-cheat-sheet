# Exercice 3 - Livre de recettes

## Instructions

A partir des maquettes suivantes ([lien](https://www.figma.com/file/ds8c9ypNtaZJYI8Hb9xGjq/Exercice-3---livre-de-recette?node-id=0%3A1&t=RToX0YOqX4wO06k7-1)) ainsi que des librairies [Antd](https://ant.design/components/overview) et [Zustand](https://docs.pmnd.rs/zustand/getting-started/introduction)

Il faudra développer un livre de recettes composé de trois pages

### Page d'accueil

La page d'accueil liste les recettes crées. Au click sur une recette, on est redirigé sur la page recette

### Page recette

La page recette montre le contenue de la recette ainsi qu'un boutton permettant de la supprimer

### Page ajouter une recette

La page ajouter une recette montre un formulaire permettant de créer une recette

### Layout

Sur toute les pages, on trouvera un Layout composé d'un `header` contenant deux boutons, un redirigeant vers la page d'accueil et l'autre vers la page d'ajout de recette.

Notes: les recettes seront stoqués dans le store de l'applications et devront être persistées si l'utilisateur recharge la page

---

# Pour aller plus loin

### Systeme de tag

sur la page recette, on trouve un bouton permettant d'ajouter un tag. Au click sur ce bouton, une modale apparait avec un champs texte.

Une fois ajouté le tag apparait sur la carte de la recette.

Sur la page d'accueil, un nouveau champs de recherche est ajouté. Un champs `select` en mode tag permet de filtrer les recettes présentes sur la page en fonction de leur tag. Si aucun tag n'est sélectionné, toute les recettes apparaissent.

---

# Correction

Vous pourrez retrouver la correction sur [ce lien](https://drive.google.com/drive/folders/1Cd5tUvyuyfdzk1BnpbBs4stA_PvQnQX_?usp=share_link)
