# Les hooks

Sur cette page, nous allons voir les principaux hooks de React et comment ils fonctionnent.

## le hook d'état: useState

Le hook `useState` permet d'ajouter une variable d'état à un composant. ([en apprendre plus sur l'état d'un composant](https://react.dev/learn/state-a-components-memory))

Le hook `useState` fonctionne de la façon suivante:

```js{8}
// Dans un composant

// il faut importer useState depuis react
import { useState } from "react";

const MyComponent = () => {
  // Ici on définie la variable d'état count qui est initialisée à 0
  const [count, setCount] = useState(0);

  return <div>{count}</div>;
};

export default MyComponent;
```

On peut voir ici qu'on à créé la variable d'état `count` qui sera donc accessible dans le composant, ainsi que la fonction `setCount` qui permettra de changer la valeur de la variable `count`

Pour modifier une variable d'état il existe deux possibilitées.

- Soit donner la nouvelle valeur directement dans la fonction `setCount`
- soit, dans le cas ou on a besoin de la valeur précédente, donner une fonction en paramètre de la fonction `setCount` qui va prendre la valeur précédente en paramètre et retourner la nouvelle valeur

```js
// ici, on définie la valeur de count à 3
setCount(3);

// ici, on modifie la valeur de count en fonction de sa valeur précédente
setCount((previousValue) => {
  return previousValue + 1;
});
```

voir la [doc](https://fr.reactjs.org/docs/hooks-state.html)

## Le hook d'effet: useEffect

Le hook useEffect permet d'exécuter de la logique tout au long du cycle de vie d'un composant. ([en savoir plus](https://fr.reactjs.org/docs/state-and-lifecycle.html))

On peut l'utiliser de cette façon:

```js
import { useEffect } from "react";

const MyComponent = () => {
  useEffect(() => {
    // S'exécute lors du chargement du composant ou lors de la
    // modification d'un des éléments du tableau de dépendances

    return () => {
      // S'exécute lors de la déstruction du composant
    };
  }, []); // Le tableau ici contient les dépendances du useEffect.
};

export default MyComponent;
```

Lorsque un élément du tableau de dépendances change le contenue du useEffect sera exécuté. Si le tableau est vide, le useEffect ne se lancera uniquement au chargement du composant.

voir la [doc](https://fr.reactjs.org/docs/hooks-effect.html)

## useContext

Le hook useContext est une fonctionnalité de React qui permet de partager des données entre différents composants de votre application, sans avoir besoin de passer ces données via les propriétés de chaque composant.

En utilisant useContext, vous pouvez créer un objet contenant les données que vous souhaitez partager, puis y accéder depuis n'importe quel composant qui en a besoin. Cela permet d'éviter de passer les mêmes données de composant en composant de manière répétitive.

Le hook useContext est particulièrement utile pour les données qui sont globales à votre application, comme les données d'authentification ou les préférences utilisateur.

Pour utiliser useContext, vous devez d'abord créer un "contexte" en utilisant la fonction createContext. Ensuite, vous pouvez utiliser le hook useContext dans un composant pour accéder aux données stockées dans ce contexte.

```js
// App.js
import { createContext } from "react";

const ThemeContext = createContext(null);

const App = () => {
  return (
    <ThemeContext.Provider value="dark">
      <Form />
    </ThemeContext.Provider>
  );
};

export default App;
```

```js
// Component.js
import { useContext } from "react";

const Component = () => {
  const theme = useContext(ThemeContext);

  return <h1>Theme de l'application: {theme}</h1>;
};
```

voir la [doc](https://react.dev/reference/react/useContext)

## Hook personnalisé

Les hooks personnalisés en React sont des fonctions JavaScript que vous pouvez créer pour encapsuler de la logique commune que vous souhaitez réutiliser dans plusieurs composants. Les hooks personnalisés sont une extension des hooks de base fournis par React, tels que useState, useEffect, useContext, etc.

Les hooks personnalisés peuvent être utilisés pour isoler la logique complexe de vos composants et les rendre plus simples et plus faciles à comprendre. Ils peuvent également aider à éliminer la duplication de code et à faciliter la maintenance de votre application.

La création d'un hook personnalisé est relativement simple. Vous pouvez créer une fonction qui utilise des hooks de base ou d'autres hooks personnalisés pour encapsuler une certaine logique. Cette fonction doit ensuite retourner un tableau ou un objet contenant les valeurs que vous souhaitez exposer aux composants.

Voici un exemple de code qui montre comment créer un hook personnalisé simple:

```js
import { useState } from "react";

const useCounter = () => {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount((previousState) => previousState + 1);
  };

  return [count, increment];
};

export default useCounter;
```

Ce hook personnalisé, appelé useCounter, utilise le hook useState pour gérer un compteur simple. La fonction retourne un tableau contenant la valeur du compteur et une fonction pour l'incrémenter.

Les composants peuvent ensuite utiliser ce hook personnalisé pour afficher le compteur:

```js
import React from "react";
import useCounter from "./useCounter";

function Counter() {
  const [count, increment] = useCounter();

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={increment}>Increment</button>
    </div>
  );
}

export default Counter;
```

En utilisant le hook personnalisé useCounter, le composant Counter peut facilement gérer un compteur en utilisant la logique encapsulée dans le hook.

:::info
A noter que pour que le hook soit bien interprété par React, il faut qu'il commence par `use` suivit d'une majuscule (par exemple: `useCounter`, `useForm`, `useOfflineDetector`...)
:::

voir la [doc](https://fr.reactjs.org/docs/hooks-custom.html)
