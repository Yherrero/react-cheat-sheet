# Ajouter du style

Il y a plusieurs façons d'ajouter du style dans une application React, voici quelques-unes des plus courantes :

- CSS Externe : L'une des méthodes les plus courantes pour ajouter du style à une application React est d'utiliser des fichiers CSS externes. Dans ce cas, les styles sont définis dans des fichiers .css séparés, qui sont ensuite importés dans les fichiers JavaScript qui contiennent les composants. Cette approche permet de séparer complètement les styles de la logique de l'application.
- Inline CSS : Une autre méthode pour ajouter du style dans une application React est d'utiliser des styles CSS en ligne. Dans ce cas, les styles sont définis directement dans les éléments HTML à l'aide de la propriété "style". Cette approche permet de définir des styles spécifiques à un élément, mais peut rendre le code moins lisible et moins maintenable.
- CSS-in-JS : Le CSS-in-JS est une méthode de création de styles qui consiste à écrire les styles directement dans le code JavaScript qui contient les composants. Cela permet de définir des styles dynamiques en fonction de l'état des composants ou des propriétés passées en paramètres. Les bibliothèques les plus populaires pour le CSS-in-JS en React sont "styled-components", "Emotion", et "JSS".
- Frameworks CSS : Il existe également des frameworks CSS populaires tels que Bootstrap, Material UI et Tailwind CSS, qui proposent des composants React préconçus avec des styles prédéfinis. Cela permet de gagner du temps et de faciliter le développement en utilisant des styles et des composants prêts à l'emploi.

La méthode choisie dépendra des préférences personnelles du développeur, des besoins de l'application et des technologies utilisées.
