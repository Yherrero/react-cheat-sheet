# Communiquer avec une API

## Utiliser fetch

Pour récupérer des données depuis une API, on peut utiliser la fonction `fetch` offerte par javascript. Par exemple, si on veut récupérer la liste des films star wars, on peut utiliser fetch sur l'api [swapi.dev](https://swapi.dev/)

```js
fetch("https://swapi.dev/api/films")
  .then((response) => response.json())
  .then((data) => {
    // traitement de la donnée
    console.log(data);
  });
```

On obtient alors dans le deuxieme `.then` le résultat de la requête. On peut donc traiter la donnée à cet endroit là pour par exemple modifier le state de notre composant pour y stoquer ce que l'on vient de recevoir.

## Faire une requête POST

Pour faire un requête POST, on peut utiliser la fonction fetch en ajoutant un objet config en deuxième paramètre

```js
const config = {
  method: "POST",
  body: JSON.stringify(data),
  headers: {
    "Content-Type": "application/json",
  },
};

fetch("https://api-url.com/api-endpoint", config);
```

Ici, on envoie donc une requête POST à notre API. Le `body` contient les données que l'on veut envoyer qui sont transformées au format JSON grâce à la méthode `JSON.stingify()`.  
La partie `headers` permet de préciser à l'API que l'on envoie bien les données sous le format JSON

### Pour plus de détails

Pour plus de détail sur comment communiquer avec une API en React (et en javascript en général) vous pouvez regarder la [doc de fetch ici](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API)
