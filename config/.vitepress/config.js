import { defineConfig } from "vitepress";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "React cheatsheet",
  description: "Aide mémoire à l'utilisation de React",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [{ text: "Home", link: "/" }],

    sidebar: [
      {
        text: "Les bases",
        items: [
          { text: "Initialiser un projet React", link: "/basics/init-project" },
          { text: "Créer un composant", link: "/basics/first-component" },
          { text: "Faire descendre de la donnée", link: "/basics/props" },
          {
            text: "Faire remonter de la donnée",
            link: "/basics/custom-events",
          },
          { text: "Les listes", link: "/basics/loops" },
          {
            text: "Le rendu conditionnel",
            link: "/basics/conditional-rendering",
          },
          {
            text: "Fonctions utiles en js",
            link: "/basics/javascript-cheatsheet",
          },
        ],
      },
      {
        text: "Notions avancées",
        items: [
          { text: "Les hooks", link: "/advanced/hooks" },
          { text: "Communiquer avec une API", link: "/advanced/fetch-api" },
          { text: "Ajouter du style", link: "/advanced/add-styles" },
        ],
      },
      {
        text: "Ajouter des librairies",
        items: [
          { text: "React Router", link: "/libraries/react-router" },
          { text: "Le store avec Zustand", link: "/libraries/zustand" },
          { text: "Styled components", link: "/libraries/styled-components" },
          { text: "Antd", link: "/libraries/ant-design" },
          { text: "Tailwind", link: "/libraries/tailwind" },
        ],
      },
      {
        text: "Exercices",
        items: [
          { text: "Liste d'utilisateurs", link: "/projects/tp1-user-list" },
          { text: "Blog", link: "/projects/tp2-blog" },
          { text: "Livre de recettes", link: "/projects/tp3-recipes" },
        ],
      },
      {
        text: "Next",
        items: [
          { text: "Setup Next project", link: "/next/setup-next-project" },
          { text: "Next routing", link: "/next/routing" },
          { text: "Next api", link: "/next/api" },
          { text: "Setup tRPC pour next", link: "/next/next-trpc" },
          {
            text: "Organisation de projet",
            link: "/next/project-organization",
          },
        ],
      },
    ],
  },
});
