# Setup a new Next project

## Liste des techno utilisées

- Next
- Typescript
- Tailwind
- Shadcn ui
- zod
- react query
- react hook form

- eslint, prettier & husky

# Partie 1: Next, Typescript, Tailwind

- initialiser le projet (penser à changer `my-app` par le nom du projet)
  `bunx create-next-app@latest my-app --typescript --tailwind --eslint`

Selectioner les choix suivants :

```
✔ Would you like to use `src/` directory? … [No]
✔ Would you like to use App Router? (recommended) … [Yes]
✔ Would you like to customize the default import alias? … [No]
```

- ajouter la config tailwind et shadcn-ui
  `bunx shadcn-ui@latest init`

Selectioner les choix suivants :

```
Would you like to use TypeScript (recommended)? [yes]
Which style would you like to use? › Default
Which color would you like to use as base color? › Slate
Where is your global CSS file? › › app/globals.css
Do you want to use CSS variables for colors? › [yes]
Where is your tailwind.config.js located? › tailwind.config.js
Configure the import alias for components: › @/components
Configure the import alias for utils: › @/lib/utils
Are you using React Server Components? › [yes]
```

## Setup eslint, prettier et husky

installer les dépendances :
`bun add -d eslint prettier eslint-config-prettier @typescript-eslint/eslint-plugin husky lint-staged prettier-plugin-tailwindcss`

lancer la commande suivante:
`yarn husky install`

puis
`yarn husky add .husky/pre-commit "yarn lint-staged"`

- Créer le fichier .prettierrc.json à la racine du projet :

```json
{
  "plugins": ["prettier-plugin-tailwindcss"],
  "semi": false,
  "trailingComma": "es5",
  "singleQuote": true,
  "tabWidth": 2,
  "useTabs": false
}
```

- Créer le fichier .eslintrc.json à la racine du projet :

```json
{
  "plugins": ["@typescript-eslint"],
  "extends": [
    "next/core-web-vitals",
    "plugin:@typescript-eslint/recommended",
    "prettier"
  "rules": {
  ],
    "@typescript-eslint/no-unused-vars": "error",
    "@typescript-eslint/no-explicit-any": "error"
  }
}
```

- Créer le fichier lint-staged.config.js à la racine du projet :

```js
module.exports = {
  // Type check TypeScript files
  "**/*.(ts|tsx)": () => "yarn tsc --noEmit",

  // Lint then format TypeScript and JavaScript files
  "**/*.(ts|tsx|js)": (filenames) => [
    `yarn eslint --fix ${filenames.join(" ")}`,
    `yarn prettier --write ${filenames.join(" ")}`,
  ],

  // Format MarkDown and JSON
  "**/*.(md|json)": (filenames) =>
    `yarn prettier --write ${filenames.join(" ")}`,
};
```

## Ajouter les librairies manquantes

- installer les librairies :
  `bun add zod @tanstack/react-query react-hook-form`

- Zod: validation de schema
- react-query: Gestion des data asynchrones
- react-hook-form: Gestion des formulaires
