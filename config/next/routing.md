# Le routing avec Next.js

::: tip
A noter que ce guide se concentre sur la version 13 de Next et plus particulierement sur l'app directory introduit avec cette version
:::

## Routing

Next js utilise un routing basé sur l'arboréscence de fichiers. Pour créer une route, il faut créer un dossier representant le nom de la route contenant un fichier `page.tsx` dans le dossier `app`

```
.
├─ app/
│  ├─ page.tsx
│  └─ dashboard/
│     └─ page.tsx
```

Dans l'exemple ci-dessus, on peut donc voir les routes `/` et `/dashboard`

On peut avoir autant de profondeur que nécessaire, par exemple si on veut créer la route `/about/user/me` il faudra créer le fichier `/app/about/user/me/page.tsx`

## Routes dynamiques

Pour créer une route dynamique (par exemple `/user/id`) il faudra nomer le dossier avec le nom du parametre entre crochet. Par exemple pour la route `/user/id` avec l'id dynamique, il faudra créer le fichier `/app/user/[id]/page.tsx`

pour accéder a la variable dynamique une fois dans le composant, il faut le récupérer directement depuis les props:

```tsx
export default function Page({ params }: { params: { id: string } }) {
  return <div>Mon is: {params.id}</div>;
}
```

on aura alors les résultats suivants:

| File Path                 | Route     | Data            |
| ------------------------- | --------- | --------------- |
| `app/blog/[slug]/page.js` | `/blog/a` | `{ slug: 'a' }` |
| `app/blog/[slug]/page.js` | `/blog/b` | `{ slug: 'b' }` |
| `app/blog/[slug]/page.js` | `/blog/c` | `{ slug: 'c' }` |

## Ignored folders

Pour organiser les différentes routes dans des dossiers sans forcement affecter les url, il faut créer des dossiers entre parentheses. Par exemple le fichier `/app/(landing)/page.tsx` sera accessible à l'url `/`

## Fichiers spéciaux

Next.js fournit un ensemble de fichiers spéciaux pour créer une ui avec un comportement spécifique dans les routes imbriquées :

- `layout`: Shared UI for a segment and its children
- `page`: Unique UI of a route and make routes publicly accessible
- `loading`: Loading UI for a segment and its children
- `not-found`: Not found UI for a segment and its children
- `error`: Error UI for a segment and its children

## En savoir plus

Pour en savoir plus la doc officielle est disponible sur [ce lien](https://nextjs.org/docs/getting-started/project-structure)
