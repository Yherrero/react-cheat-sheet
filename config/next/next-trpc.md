# Communication front/back avec tRPC

## Objectif du guide

- Comprendre l'interet de tRPC
- Mettre en place tRPC sur un projet next
- Utiliser tRPC dans ses composants client et serveur

## Pourquoi utiliser trpc ?

tRPC (typescript RPC) est une librairie de remote procedure call qui révolutionne le développement d'API en comblant de manière transparente les communications entre le client et le serveur.

tRPC est designé de façon à simplifier le développement d'API et permet la communication client-serveur des projets Typescript. tRPC propose une manière type safe de définir des API endpoints, de gérer les requetes et la validations des inputs. Avec tRPC, on peut définir la structure de son API avec des types Typescript, ce qui permet d'assurer que le client comme le serveur sont bien coordonés en ce qui concerne les données disponibles et nécessaires des différentes opérations.

tRPC permet d'avoir du typage et de l'autocompletion grâce à Typescript, ce qui assure des échanges type safe et un feedback instantané lors du développement des communication client-serveur.

## Exemple

Voici un exemple minimal d'une communication client-serveur. Dans cet exemple, le serveur propose une route `greeting` qui permet de retourner un objet contenant un message qui salue l'utilisateur, ou qui serait `Hello World` si aucun name n'est envoyé.
Le client affiche le résultat de l'appel api en lui passant un paramètre.

```ts
// server.ts
import { initTRPC } from "@trpc/server";
import { z } from "zod";

const t = initTRPC.create();

const appRouter = t.router({
  greeting: t.procedure
    .input(
      z.object({
        name: z.string().optional(),
      })
    )
    .query(({ input }) => {
      return {
        message: `Hello ${input.name ?? "World"}`,
      };
    }),
});

export type AppRouter = typeof appRouter;
```

```ts
// client.ts
import { trpc } from "@/app/_trpc/client";

export default function MyComponent() {
  const greeting = trpc.greeting.useQuery({ name: "Yannick" });

  return <div>{greeting.data?.message}</div>;
}
```

L'avantage de tRPC par rapport à une api classique et que sans avoir besoin de définir des types et des interfaces supplémentaires, les communications client-serveur sont déjà complettement typées. On a bien l'autocompletion et le linting qui fonctionnent correctement.

## Mise en place de tRPC sur un projet Next

### Installer les dépendances

Dans un projet next 13, une fois le projet initialisé (voir le processus d'initialisation du projet) il faudra installer les packages suivants:

On va ici installer `@trpc/server` pour mettre en place tRPC côté serveur, `@trpc/client` pour pouvoir utiliser tRPC côté client et enfin on va utiliser tanstack react-query par dessus tRPC, c'est pourquoi on va aussi installer `@trpc/react-query` et `@tanstack/react-query`.

```sh
bun add @trpc/server @trpc/client @trpc/react-query @tanstack/react-query
```

### Setup le serveur tRPC

Pour mettre en place la partie serveur de tRPC, il faudra créer le dossier `server` à la racine de notre application (ou dans le dossier `src` si vous avez choisis de l'utiliser à l'initialisation du projet)
Dans ce dossier `server`, on va créer le fichier `trpc.ts`

```ts
import { initTRPC } from "@trpc/server";

const t = initTRPC.create();

export const router = t.router;
export const publicProcedure = t.procedure;
```

Une fois que le serveur tRPC à été créer, on va créer notre router tRPC. Dans le même dossier `server`, on va créer le fichier `index.ts`

```ts
import { publicProcedure, router } from "./trpc";

export const appRouter = router({
  greeting: publicProcedure.query(async () => {
    return "hello world";
  }),
});

export type AppRouter = typeof appRouter;
```

On vient ici de créer notre router tRPC contenant pour le moment une seule procédure `greeting`.

Pour connecter cet appRouter au router de Next, il faut alors créer le fichier `app/api/trpc/[trpc]/route.ts`

```ts
import { fetchRequestHandler } from "@trpc/server/adapters/fetch";

import { appRouter } from "@/server";

const handler = (req: Request) =>
  fetchRequestHandler({
    endpoint: "/api/trpc",
    req,
    router: appRouter,
    createContext: () => ({}),
  });

export { handler as GET, handler as POST };
```

la structure des nouveaux fichiers de l'application est donc la suivante:

```
.
├── app
│   └─ api
│      └─ trpc
│         └─ [trpc]
│             └─ route.ts
│
├── server
│   ├── index.ts
│   └── trpc.ts
```

Et voila, une fois tout cela mis en place, si on lance l'application next avec `bun run dev`, on pourra alors accéder à la route `localhost:3000/api/trpc/greeting`

pour ajouter d'autres routes, il suffira alors de créer d'autre procédures et de les ajouter au fichier `server/index.ts`

## Utiliser tRPC dans les composants clients

Pour pouvoir utiliser les procédures tRPC depuis les composants client, il nous faudra créer le fichier `app/_trpc/client.ts` qui contiendra simplement ceci:

```ts
import { createTRPCReact } from "@trpc/react-query";

import { type AppRouter } from "@/server";

export const trpc = createTRPCReact<AppRouter>({});
```

Pour pouvoir utiliser ce client dans notre application, il nous faut rajouter un provider. pour cela, on va créer dans le même dossier le fichier `provider.tsx`

```ts
"use client";

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { httpBatchLink } from "@trpc/client";
import { useState } from "react";

import { trpc } from "./client";

export function Providers({ children }: { children: React.ReactNode }) {
  const [queryClient] = useState(() => new QueryClient({}));
  const [trpcClient] = useState(() =>
    trpc.createClient({
      links: [
        httpBatchLink({
          url: "http://localhost:3000/api/trpc",
        }),
      ],
    })
  );

  return (
    <trpc.Provider client={trpcClient} queryClient={queryClient}>
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    </trpc.Provider>
  );
}
```

Une fois le provider créer, il faut l'utiliser dans notre layout. Dans le fichier `app/layout.tsx`, utiliser le code suivant:

```ts
import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";

import { Providers } from "./providers";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "My App",
  description: "Apprendre l'anglais",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Providers>{children}</Providers>
      </body>
    </html>
  );
}
```

une fois tout ceci fait, il suffira simplement d'importer le client tRPC dans nos composants pour pouvoir l'utiliser avec react-query. Par exemple dans le composant `my-component.tsx` on aurait le code suivant:

```ts
'use client'

import { trpc } from '@/app/_trpc/client'

export default function MyComponent() {
  const dashboardData = trpc.getDashboardData.useQuery()

  return <div>{greeting.data}</div>
```

A partir d'ici, on aura accès à toute les typages et l'autocomplétion de nos query pour les paramètres comme pour les réponses.

## Utiliser tRPC dans les server components (et le ssr)

Pour utiliser tRPC dans nos composants serveur, il faudra créer le serverClient dans le même dossier que notre client.ts
On va donc créer le fichier `app/_trpc/serverClient.ts`

```ts
import { httpBatchLink } from "@trpc/client";

import { appRouter } from "@/server";

export const serverClient = appRouter.createCaller({
  links: [
    httpBatchLink({
      url: "http://localhost:3000/api/trpc",
    }),
  ],
});
```

Pas besoin de plus. Une fois le serverClient créer, on peut directement l'utiliser dans nos server components comme ceci

```ts
import { serverClient } from "@/app/_trpc/_serverClient";

export default async function MyServerComponent() {
  const greeting = await serverClient.greeting();

  return <div>{greeting}</div>;
}
```

## Bravo, tout est fonctionnel

Pour en savoir plus sur comment utiliser tRPC (création de procédures, validation d'input etc) la doc est disponible sur [ce lien](https://trpc.io/docs/server/procedures)
