---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "React cheatsheet"
  text: "Aide mémoire à l'utilisation de React"
  tagline: Formation React - Acelys
  actions:
    - theme: brand
      text: Apprendre les bases
      link: /basics/init-project
    - theme: alt
      text: Notions avancées
      link: /advanced/hooks

features:
  - title: Les bases
    details: Initialisation de projet, créer un composant et faire transiter les données
  - title: Notions avancées
    details: Ajouter du style, utiliser les hooks et communiquer avec des API externes
  - title: Les outils externes
    details: React router pour le router, Zustand pour le store et Styled-components pour le style
---
