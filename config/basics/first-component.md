# Créer un composant

En React, un composant est juste une fonction qui prend en paramètre des props et qui retourne du jsx.

Pour définir un composant, il faut utiliser la syntaxe suivante:

```js
// src/components/MyComponent.js

const MyComponent = (props) => {
  return <div>Hello world</div>;
};

export default MyComponent;
```

pour utiliser ensuite ce composant, il faudra l'importer puis l'utiliser comme une balise HTML, par exemple :

```js{3}
<div>
  <h2>Un autre composant</h2>
  <MyComponent />
</div>
```

Dans la grande majorité des cas, on ne créera qu'un seul composant par fichier et ils porteront le même nom.

La plupart des composants qu'on aura à développer se trouveront dans le dossier `src/components` et pourront être trié dans des sous-dossiers si besoin. (Excéption faite des composants Pages, que l'on verra dans la partie [react-router](/libraries/react-router))

# Le JSX

Le JSX est un outil fournit par React pour nous permettre de décrire à quoi devrait ressembler l’interface utilisateur. Pour plus de détails, voir [ce lien](https://fr.reactjs.org/docs/introducing-jsx.html)
