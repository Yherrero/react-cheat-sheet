# Les liste

Dans le code suivant, on utilise la méthode `map()` pour prendre un tableau de nombres et doubler leurs valeurs. On affecte le nouveau tableau retourné par `map()` à une variable doubled et on l’affiche dans la console :

```js
const numbers = [1, 2, 3, 4, 5];

const doubled = numbers.map((number) => number * 2);

console.log(doubled);
```

Ce code affiche `[2, 4, 6, 8, 10]` dans la console.

Avec React, transformer un tableau en une liste d’éléments est presque identique.

Pour se faire, dans notre composant on peut utiliser la fonction `map()` directement dans notre JSX pour afficher notre liste

```js
// MyList.js

const MyList = () => {
  const items = [1, 2, 3, 4, 5];

  return (
    <ul>
      {items.map((number) => (
        <li key={number}>{number}</li>
      ))}
    </ul>
  );
};
```

Un point très important à noter est qu'il faut définir une clé `key` unique à chaque élément de la liste qui permettra à React d'effectuer plusieurs optimisations et aussi d'éviter certains bugs.

[En apprendre plus](https://fr.reactjs.org/docs/lists-and-keys.html#keys)
