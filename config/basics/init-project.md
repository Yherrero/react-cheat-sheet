# Initialiser un projet React

Pour initialiser un projet en React, vous pouvez utiliser un outil appelé "Create React App". Il s'agit d'une commande en ligne qui génère automatiquement une structure de projet React prête à l'emploi avec les outils de compilation et de développement nécessaires.

Voici les étapes à suivre pour initialiser un projet en React avec Create React App :

1. Tout d'abord, assurez-vous que vous avez Node.js et npm installés sur votre ordinateur.

2. Ouvrez votre terminal ou votre invite de commande et exécutez la commande suivante pour installer Create React App de manière globale sur votre ordinateur :

```sh
npm install -g create-react-app
```

3. Une fois l'installation terminée, créez un nouveau projet React en exécutant la commande suivante dans votre terminal :

```sh
create-react-app nom-de-votre-projet
```

Assurez-vous de remplacer "nom-de-votre-projet" par le nom que vous souhaitez donner à votre projet.

:::info
A noter qu'il est possible de se passer d'installer `create-react-app` globalement sur votre machine en utilisant la commande `npx create-react-app nom-de-votre-projet`
:::

4. La commande Create React App créera automatiquement une structure de projet de base pour vous. Une fois la commande terminée, vous pouvez vous déplacer dans le répertoire du projet en exécutant la commande suivante :

```sh
cd nom-de-votre-projet
```

5. Ensuite, vous pouvez démarrer le serveur de développement en exécutant la commande suivante :

```sh
npm start
```

Cela lancera le serveur de développement et ouvrira automatiquement une fenêtre de navigateur avec votre application React en cours d'exécution. Vous pouvez maintenant commencer à travailler sur votre projet et à ajouter vos composants et vos fonctionnalités.
