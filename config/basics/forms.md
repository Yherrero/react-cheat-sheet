# Les formulaires

Les formulaires HTML fonctionnent un peu différemment des autres éléments du DOM en React car ils possèdent naturellement un état interne. Par exemple, ce formulaire en HTML qui accepte juste un nom :

```html
<form>
  <label>
    Nom :
    <input type="text" name="name" />
  </label>
  <input type="submit" value="Envoyer" />
</form>
```

Ce formulaire a le comportement classique d’un formulaire HTML et redirige sur une nouvelle page quand l’utilisateur le soumet. Si vous souhaitez ce comportement en React, vous n’avez rien à faire. Cependant, dans la plupart des cas, vous voudrez pouvoir gérer la soumission avec une fonction JavaScript, qui accède aux données saisies par l’utilisateur. La manière classique de faire ça consiste à utiliser les « composants contrôlés ».

## Composants contrôlés

En HTML, les éléments de formulaire tels que `<input>`, `<textarea>`, et `<select>` maintiennent généralement leur propre état et se mettent à jour par rapport aux saisies de l’utilisateur. En React, l’état modifiable est généralement stocké dans le state du composant et n'est modifiable que grâce aux setter

On peut combiner ces deux concepts en utilisant l’état local React comme « source unique de vérité ». Ainsi le composant React qui affiche le formulaire contrôle aussi son comportement par rapport aux saisies de l’utilisateur. Un champ de formulaire dont l’état est contrôlé de cette façon par React est appelé un « composant contrôlé ».

Par exemple, en reprenant le code ci-dessus pour afficher le nom lors de la soumission, on peut écrire le formulaire sous forme de composant contrôlé :

```js
const NameForm = () => {
  const [value, setValue] = useState('')

  changeHandler(event) {
    setValue(event.target.value);
  }

  submitHandler(event) {
    event.preventDefault();
    alert('Le nom a été soumis : ' + this.state.value);
  }

  return (
    <form onSubmit={submitHandler}>
      <label>
        Nom :
        <input type="text" value={value} onChange={changeHandler} />
      </label>
      <button type="submit">Envoyer</button>
    </form>
  )

}
```

À présent que l’attribut value est défini sur notre élément de formulaire, la valeur affichée sera toujours celle de `value`(du state), faisant ainsi de l’état local React la source de vérité. Puisque `changeHandler` est déclenché à chaque frappe pour mettre à jour l’état local React, la valeur affichée restera mise à jour au fil de la saisie.

Dans un composant contrôlé, la valeur du champ est en permanence pilotée par l’état React. Même si ça signifie que vous devez taper un peu plus de code, vous pouvez désormais passer la valeur à d’autres éléments de l’UI, ou la réinitialiser depuis d’autres gestionnaires d’événements.
