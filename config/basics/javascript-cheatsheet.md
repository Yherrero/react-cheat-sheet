# Fonctions utiles en javascript:

[le destructuring](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

### Les Array functions

- [forEach](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)
- [map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)
- [filter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)
- [reduce](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce)
- [sort](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)

::: warning
Attention, contrairement aux autres array functions la fonction `sort` ne créé pas un nouveau tableau, elle modifie le tableau sur lequel elle est appliquée
:::

### La destructuration

La destructuration en JavaScript est une fonctionnalité qui permet d'extraire des valeurs à partir d'objets ou de tableaux de manière plus concise. Cela permet d'éviter de répéter des expressions et facilite la manipulation de données complexes.

Prenons un exemple avec un objet:

```js
const personne = {
  nom: "Dupont",
  prenom: "Jean",
  age: 30,
};
```

Pour extraire les valeurs de l'objet, on peut utiliser la destructuration de la manière suivante :

```js
const { nom, prenom, age } = personne;

console.log(nom); // Dupont
console.log(prenom); // Jean
console.log(age); // 30
```

La destructuration permet de définir des variables correspondantes aux propriétés de l'objet. On peut ainsi accéder aux valeurs de l'objet de manière plus concise.

De même, pour un tableau, la destructuration permet d'extraire les éléments de manière plus simple. Par exemple :

```js
const fruits = ["pomme", "banane", "orange"];

const [fruit1, fruit2, fruit3] = fruits;

console.log(fruit1); // pomme
console.log(fruit2); // banane
console.log(fruit3); // orange
```

La destructuration permet de définir des variables correspondantes aux éléments du tableau. On peut ainsi accéder aux éléments de manière plus concise.

La destructuration est une fonctionnalité très utile en JavaScript, elle permet de simplifier la manipulation de données complexes et d'éviter de répéter des expressions.

### Le spread operator

Le spread operator en JavaScript est une fonctionnalité qui permet de copier les valeurs d'un tableau ou d'un objet dans un nouvel objet ou tableau. Cela facilite la manipulation de données et permet de simplifier le code.

Prenons un exemple avec un tableau :

```js
const nombres = [1, 2, 3];

const nouveauxNombres = [...nombres, 4, 5];

console.log(nouveauxNombres); // [1, 2, 3, 4, 5]
```

Le spread operator permet ici de copier les valeurs du tableau `nombres` dans le nouveau tableau `nouveauxNombres` en utilisant `...` avant le nom du tableau d'origine. On peut ensuite ajouter d'autres valeurs à la suite.

De même, le spread operator peut être utilisé avec des objets :

```js
const personne = {
  nom: "Dupont",
  prenom: "Jean",
  age: 30,
};

const nouvellePersonne = { ...personne, age: 31 };

console.log(nouvellePersonne); // { nom: "Dupont", prenom: "Jean", age: 31 }
```

Le spread operator permet ici de copier les propriétés de l'objet `personne` dans un nouvel objet `nouvellePersonne`, et de modifier la propriété `age` pour qu'elle ait la valeur `31`.

### Le rest operator

Le rest operator en JavaScript est une fonctionnalité qui permet de regrouper des paramètres en une seule variable dans les fonctions. Cela permet de traiter facilement un nombre variable d'arguments passés à une fonction et de simplifier le code.

Prenons un exemple de fonction :

```js
function addition(...nombres) {
  let somme = 0;
  for (const nombre of nombres) {
    somme += nombre;
  }
  return somme;
}

console.log(addition(1, 2, 3, 4)); // 10
```

Le rest operator permet ici de regrouper les paramètres passés à la fonction `addition` en une seule variable `nombres`. On peut ainsi passer un nombre variable d'arguments à la fonction sans avoir à les énumérer individuellement dans la déclaration de la fonction.

Dans cet exemple, `...nombres` regroupe les arguments `1`, `2`, `3`, `4` en un tableau `[1, 2, 3, 4]`. On peut ensuite itérer sur ce tableau pour calculer la somme des nombres passés en argument.

Le rest operator peut également être utilisé pour extraire les derniers éléments d'un tableau :

```js
const [fruit1, fruit2, ...autresFruits] = [
  "pomme",
  "banane",
  "orange",
  "kiwi",
  "fraise",
];

console.log(fruit1); // "pomme"
console.log(fruit2); // "banane"
console.log(autresFruits); // ["orange", "kiwi", "fraise"]
```

Le rest operator permet ici de regrouper tous les éléments du tableau à partir de la troisième position dans la variable `autresFruits`. On peut ainsi extraire les premiers éléments du tableau dans des variables séparées et récupérer les autres éléments dans un tableau.

On remarque que le rest et le spread operator sont deux fonctionnalités étroitement liées, mais qui ont des utilisations légèrement différentes. le spread operator est utilisé pour décomposer une collection de données en éléments individuels, tandis que le rest operator est utilisé pour rassembler un nombre variable d'arguments passés à une fonction en un seul paramètre.
