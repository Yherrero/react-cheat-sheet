# Faire remonter de la donnée

Pour faire remonter une donnée depuis un composant enfant vers un composant parent, il faudra créer ce que l'on appelle des **custom events**

Il s'agit d'une props qui est en fait une fonction. Cette fonction est définie dans le composant parent et est ensuite donnée au composant enfant qui va l'exécuter pour faire transiter des donner.

Par exemple, on a les deux composants suivants:

```js
// Widget.js

const Widget = () => {
  return (
    <div>
      <h2>Composant englobant mon bouton</h2>
      <MyForm />
    </div>
  );
};
```

```js
// MyForm.js

const MyForm = () => {
  const formData = {
    userInput: "donnée",
  };

  return (
    <form>
      // ...
      <button>Envoyer</button>
    </form>
  );
};
```

J'aimerais faire en sorte que lorsque je clique sur le bouton, l'objet `formData` soient envoyées depuis le composant `MyForm` vers le composant `Widget`.

Pour ce faire, il faut alors créer une fonction dans le composant `Widget` pour le donner en props au composant `MyForm` qui l'utilisera alors, au moment ou l'utilisateur cliquera sur le bouton.

```js
// Widget.js

const Widget = () => {
  const submitHandler = (data) => {
    console.log(data);
  };

  return (
    <div>
      <h2>Composant englobant mon bouton</h2>
      <MyForm onSubmit={submitHandler} />
    </div>
  );
};
```

```js
// MyForm.js

const MyForm = ({ onSubmit }) => {
  const formData = {
    userInput: "donnée",
  };

  const clickHandler = () => {
    // Ici, on utilise la fonction définie par le
    // composant parent pour faire transiter formData
    onSubmit(formData);
  };

  return (
    <form>
      // ...
      <button onClick={clickHandler}>Envoyer</button>
    </form>
  );
};
```

[Pour en apprendre plus](https://fr.reactjs.org/docs/lifting-state-up.html)
