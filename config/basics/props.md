# Faire descendre de la donnée

Pour faire descendre une donnée d'un composant parent à un composant enfant, on peut utiliser les props.

Imaginons que je suis dans mon composant `Widget` et que je veux envoyer le nom de l'utilisateur connecté au composant enfant `UserDetail`. Pour faire cela, je peux utiliser procéder comme ceci:

```js{12}
// Widget.js

const Widget = () => {
  const user = {
    name: 'John',
    age: '27',
  }

  return (
    <div>
      <h2>Mon Widget</h2>
      <UserDetail user={user}>
    </div>
  )
}

export default Widget
```

On voit ici qu'on a rajouté la propriété `user={user}` et donc, le composant `UserDetail` pourra donc avoir accès à `user` dans ses props.

```js
// UserDetail.js

const UserDetail = (props) => {
  return (
    <div>
      <h2>{props.user.name}</h2>
      <p>{props.user.age}</p>
    </div>
  );
};

export default UserDetail;
```

A noter qu'il est possible d'utiliser la décomposition (destructuring) ici pour avoir une vision plus claire des différentes props que l'on va récupérer ([plus de détails ici](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment))

```js
// UserDetail.js

const UserDetail = ({ user }) => {
  return (
    <div>
      <h2>{user.name}</h2>
      <p>{user.age}</p>
    </div>
  );
};

export default UserDetail;
```

[Pour en apprendre plus](https://fr.reactjs.org/docs/components-and-props.html#function-and-class-components)
