# Le rendu conditionnel

Avec React, il est possible dans son JSX d'afficher certains éléments uniquement si une condition est respectée.

On peut voir trois principales façon d'utiliser un rendu conditionnel qui sont les suivantes:

```js
// dans un composant
<div>
  { condition ? <ComponentA /> : <ComponentB> }
</div>
```

Dans cet exemple, on utilise une condition ternaire pour afficher `<ComponentA>` si la condition est vraie et `<ComponentB>` si elle est fausse

---

```js
// dans un composant
<div>{condition && <Component />}</div>
```

Dans cet exemple, on utilise l'operateur `&&` qui permet d'afficher ce qui vient après (ici le `<Component />`) si et seulement si la condition est vraie. Cette notation est utile si on veut afficher un élément si une condition est respectée et qu'on ne veut rien afficher dans le cas contraire

---

```js
// MyComponent

const MyComponent = () => {
  const condition = true;

  if (condition) {
    return (
      <div>
        <h2>La condition est vraie</h2>
        <Component />
      <div>
    )
  }

  return return (
      <div>
        <h2>La condition est vraie</h2>
      <div>
    )

}
```

Dans ce dernier exemple, on peut voir que cette fois, c'est tout le return du composant qui dépend de la condition. Cette façon de faire est utile si jamais on veut éviter d'avoir une condition ternaire au milieu de notre return pour gagner en lisibilité dans le code.

[Pour en apprendre plus](https://fr.reactjs.org/docs/conditional-rendering.html)
